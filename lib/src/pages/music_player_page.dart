import 'package:animate_do/animate_do.dart';
import 'package:assets_audio_player/assets_audio_player.dart';
import 'package:flutter/material.dart';
import 'package:musicplayer/src/helpers/helpers.dart';
import 'package:musicplayer/src/models/audio_player_model.dart';
import 'package:musicplayer/src/widgets/custom_appbar_widget.dart';
import 'package:provider/provider.dart';

class MusicPlayerPage extends StatelessWidget {
  const MusicPlayerPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Stack(
      children: <Widget>[
        Background(),
        Column(
          children: <Widget>[
            CustomAppBar(),
            _ImageDiscDuration(),
            PlayTitle(),
            Lyrics()
          ],
        ),
      ],
    ));
  }
}

class Background extends StatelessWidget {
  const Background({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final screenSize = MediaQuery.of(context).size;

    return Container(
      width: double.infinity,
      height: screenSize.height * 0.61,
      decoration: BoxDecoration(
          borderRadius: BorderRadius.only(bottomLeft: Radius.circular(40)),
          gradient: LinearGradient(
              begin: Alignment.centerLeft,
              end: Alignment.center,
              colors: [Color(0xff33333E), Color(0xff201E28)])),
    );
  }
}

class Lyrics extends StatelessWidget {
  const Lyrics({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final lyrics = getLyrics();

    return Expanded(
        child: ListWheelScrollView(
      physics: BouncingScrollPhysics(),
      itemExtent: 42,
      diameterRatio: 1.5,
      children: lyrics
          .map((e) => Text(
                e,
                textAlign: TextAlign.center,
                style: TextStyle(
                    fontSize: 19, color: Colors.white.withOpacity(0.65)),
              ))
          .toList(),
    ));
  }
}

class PlayTitle extends StatefulWidget {
  const PlayTitle({
    Key? key,
  }) : super(key: key);

  @override
  State<PlayTitle> createState() => _PlayTitleState();
}

class _PlayTitleState extends State<PlayTitle>
    with SingleTickerProviderStateMixin {
  bool isPlaying = false;
  bool fisrtTime = true;
  late AnimationController playAnimation;

  final assetAudioPlayer = AssetsAudioPlayer();

  @override
  void initState() {
    playAnimation =
        AnimationController(vsync: this, duration: Duration(milliseconds: 500));
    super.initState();
  }

  @override
  void dispose() {
    playAnimation.dispose();
    super.dispose();
  }

  void open() {
    final audioPlayerModel =
        Provider.of<AudioPlayerModel>(context, listen: false);
    assetAudioPlayer.open(
        Audio('assets/Shape of You - Ed Sheeran.mp3',
            metas: Metas(
                title: "Shape of you",
                artist: "Ed Shereman",
                album: "Ed Shereman",
                image: MetasImage.asset("assets/paper_shape_of_you.jpg"))),
        showNotification: true,
        autoStart: true);
    assetAudioPlayer.currentPosition.listen((duration) {
      audioPlayerModel.current = duration;
    });

    assetAudioPlayer.current.listen((playingAudio) {
      audioPlayerModel.songDuration =
          playingAudio?.audio.duration ?? Duration(seconds: 0);
    });
  }

  @override
  Widget build(BuildContext context) {
    //Providers
    final audioPlayerModel =
        Provider.of<AudioPlayerModel>(context, listen: false);

    return Container(
      margin: EdgeInsets.only(top: 25, bottom: 35),
      padding: EdgeInsets.symmetric(horizontal: 25),
      child: Row(
        children: <Widget>[
          Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                'Shape of You',
                style: TextStyle(
                    fontSize: 25, color: Colors.white.withOpacity(0.9)),
              ),
              Text(
                'Ed Shereman',
                style: TextStyle(
                    fontSize: 15, color: Colors.white.withOpacity(0.6)),
              ),
            ],
          ),
          Spacer(),
          FloatingActionButton(
              elevation: 0,
              highlightElevation: 0,
              backgroundColor: Color(0xffF8CB51),
              child: AnimatedIcon(
                icon: AnimatedIcons.play_pause,
                progress: playAnimation,
              ),
              onPressed: () {
                if (isPlaying) {
                  playAnimation.reverse();
                  isPlaying = false;
                  audioPlayerModel.controller.stop();
                } else {
                  playAnimation.forward();
                  isPlaying = true;
                  audioPlayerModel.controller.repeat();
                }

                if (fisrtTime) {
                  open();
                  fisrtTime = false;
                } else {
                  assetAudioPlayer.playOrPause();
                }
              })
        ],
      ),
    );
  }
}

class _ImageDiscDuration extends StatelessWidget {
  const _ImageDiscDuration({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 20),
      margin: EdgeInsets.only(top: 35),
      child: Row(
        children: <Widget>[
          SizedBox(
            width: 10,
          ),
          ImageDisco(),
          Expanded(
            child: Container(),
          ),
          Progressbar(),
          SizedBox(
            width: 10,
          )
        ],
      ),
    );
  }
}

class ImageDisco extends StatelessWidget {
  const ImageDisco({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final audioPlayerModel = Provider.of<AudioPlayerModel>(context);

    return Container(
      width: 200,
      height: 200,
      child: ClipRRect(
        borderRadius: BorderRadius.circular(200),
        child: Stack(
          alignment: Alignment.center,
          children: <Widget>[
            SpinPerfect(
                duration: Duration(seconds: 10),
                infinite: true,
                controller: (animationController) =>
                    audioPlayerModel.controller = animationController,
                manualTrigger: true,
                child:
                    Image(image: AssetImage('assets/paper_shape_of_you.jpg'))),
            Container(
              width: 25,
              height: 25,
              decoration: BoxDecoration(
                  color: Colors.black26,
                  borderRadius: BorderRadius.circular(100)),
            ),
            Container(
              width: 18,
              height: 18,
              decoration: BoxDecoration(
                  color: Color(0xff1C1C25),
                  borderRadius: BorderRadius.circular(100)),
            )
          ],
        ),
      ),
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(200),
          gradient: LinearGradient(begin: Alignment.topLeft, colors: [
            Color(0xff484750),
            Color(0xff1E1C24),
          ])),
    );
  }
}

class Progressbar extends StatelessWidget {
  const Progressbar({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final style = TextStyle(color: Colors.white.withOpacity(0.4));
    final audioPlayerModel =
        Provider.of<AudioPlayerModel>(context, listen: true);
    final porcentaje = audioPlayerModel.procentaje;

    return Column(
      children: <Widget>[
        Text(audioPlayerModel.songtotalDuration, style: style),
        Stack(
          children: <Widget>[
            Container(
                width: 3.0,
                height: 200.0,
                color: Colors.white.withOpacity(0.1)),
            Positioned(
              bottom: 0,
              child: Container(
                  width: 3.0,
                  height: 200 * porcentaje,
                  color: Colors.white.withOpacity(0.8)),
            )
          ],
        ),
        Text(audioPlayerModel.currentSecond, style: style)
      ],
    );
  }
}

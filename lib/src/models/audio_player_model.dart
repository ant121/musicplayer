import 'package:flutter/material.dart';

class AudioPlayerModel with ChangeNotifier {
  bool _playing = false;
  Duration _songDuration = Duration(milliseconds: 0);
  Duration _current = Duration(milliseconds: 0);
  late AnimationController controller;

  bool get playing => _playing;
  set playing(bool valor) {
    _playing = valor;
    notifyListeners();
  }

  Duration get songDuration => _songDuration;
  set songDuration(Duration valor) {
    _songDuration = valor;
    notifyListeners();
  }

  Duration get current => _current;
  set current(Duration valor) {
    _current = valor;
    notifyListeners();
  }

  double get procentaje => songDuration.inSeconds > 0
      ? current.inSeconds / songDuration.inSeconds
      : 0;

  String get songtotalDuration => printDuration(_songDuration);
  String get currentSecond => printDuration(_current);

  String printDuration(Duration duration) {
    String twoDigits(int n) {
      if (n >= 10) {
        return "$n";
      } else {
        return "0$n";
      }
    }

    String twoDigitsMinutes = twoDigits(duration.inMinutes.remainder(60));
    String twoDigitsSeconds = twoDigits(duration.inSeconds.remainder(60));
    return "$twoDigitsMinutes:$twoDigitsSeconds";
  }

  // set controllerAnimation(AnimationController valor) {
  //   controller = valor;
  // }

  // AnimationController get controllerAnimation => controller;
}

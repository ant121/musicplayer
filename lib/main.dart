import 'package:flutter/material.dart';
import 'package:musicplayer/src/models/audio_player_model.dart';
import 'package:musicplayer/src/pages/music_player_page.dart';
import 'package:musicplayer/src/theme/theme.dart';
import 'package:provider/provider.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider(create: (_) => AudioPlayerModel()),
      ],
      child: MaterialApp(
          debugShowCheckedModeBanner: false,
          theme: miTema,
          title: 'Best Player',
          home: MusicPlayerPage()),
    );
  }
}
